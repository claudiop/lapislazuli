BOT_NAME = 'LapisLazuli'

SPIDER_MODULES = [
    'spiders.publico',
    'spiders.observador',
    'spiders.lusa',
    'spiders.cmjornal',
    'spiders.destak',
    'spiders.expresso',
    'spiders.dnjornal',
]

ITEM_PIPELINES = {
    'pipelines.MongoPipeline': 800,
}

EXTENSIONS = {
    'scrapy.extensions.closespider.CloseSpider': 100
}

ROBOTSTXT_OBEY = True
AUTOTHROTTLE_ENABLED = True
CONCURRENT_ITEMS = 100
CONCURRENT_REQUESTS_PER_IP = 16
CONCURRENT_REQUESTS_PER_DOMAIN = 16

CLOSESPIDER_ERRORCOUNT = 3

RETRY_HTTP_CODES = [500, 502, 503, 504, 522, 524, 408, 429, 403]
RETRY_PRIORITY_ADJUST = 99

LOG_LEVEL = 'INFO'

DOWNLOADER_MIDDLEWARES = {
    'scrapy.contrib.downloadermiddleware.useragent.UserAgentMiddleware': None,
    'random_useragent.RandomUserAgentMiddleware': 400,
}

USER_AGENT_LIST = "useragents"