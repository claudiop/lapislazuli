from semantic import types
from semantic.schemas.organization import NewsMediaOrganization

publico = NewsMediaOrganization()
publico.add("name", types.Text("Jornal Público"))
publico.add("url", types.URL("https://www.publico.pt/"))
publico.add("email", types.Text("publico@publico.pt"))
publico.add("email", types.Text("assinaturas.online@publico.pt"))
publico.add("email", types.Text("recursos.humanos@publico.pt"))
publico.add("telephone", types.Text("+351 210111000"))
publico.add("telephone", types.Text("+351 226151000"))
publico.add("address", types.Text("Edifício Diogo Cão, Doca de Alcântara Norte 1350-352 Lisboa"))
publico.add("address", types.Text("Rua Júlio Dinis, nº 270 Bloco A 3º 4050-318 Porto"))

lusa = NewsMediaOrganization()
lusa.add("name", types.Text("Agência Lusa"))
lusa.add("url", types.URL("https://www.lusa.pt/"))

reuters = NewsMediaOrganization()
reuters.add("name", types.Text("Reuters"))
reuters.add("url", types.URL("https://www.reuters.com/"))
