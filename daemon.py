import datetime
import logging
import settings
from time import sleep
from multiprocessing import Process
from scrapy.settings import Settings
from scrapy.crawler import CrawlerProcess
from spiders.cmjornal import CMJornalSpider
from spiders.jn import JNSpider
from spiders.nit import NitSpider
from spiders.publico import PublicoSpider
from spiders.observador import ObservadorSpider
from spiders.dnjornal import DNJornalSpider
from spiders.expresso import ExpressoSpider
from spiders.sapo import SapoSpider
from spiders.destak import DestakSpider
from spiders.lusa import LusaSpider
from spiders.rtp import RtpSpider

logging.getLogger('scrapy.spidermiddlewares.httperror').setLevel("WARN")


def quick_update():
    crawler_settings = Settings()
    crawler_settings.setmodule(settings)
    process = CrawlerProcess(crawler_settings)
    process.crawl(NitSpider, 'nounce-val', quick=True)
    process.crawl(PublicoSpider, max_page=5)
    process.crawl(JNSpider, 'cookie-val', max_page=20)
    process.crawl(
        CMJornalSpider,
        min_date=datetime.datetime.now().date() - datetime.timedelta(days=1))
    process.crawl(
        PublicoSpider,
        min_date=datetime.datetime.now() - datetime.timedelta(days=1))
    process.crawl(
        ObservadorSpider,
        min_date=datetime.datetime.now().date() - datetime.timedelta(days=1))
    process.crawl(
        DNJornalSpider,
        min_date=datetime.datetime.now().date() - datetime.timedelta(days=1))
    process.crawl(
        ExpressoSpider,
        min_date=datetime.datetime.now().date() - datetime.timedelta(days=1))
    process.crawl(
        SapoSpider,
        min_date=datetime.datetime.now().date() - datetime.timedelta(days=1))
    # process.crawl(RtpSpider, min_id=?, man_id=?)
    # process.crawl(DestakSpider, min_id=?, man_id=?)
    # process.crawl(LusaSpider, min_id=?, max_id=?)
    process.start()


if __name__ == "__main__":
    while True:
        p = Process(target=quick_update)
        p.start()
        p.join()
        sleep(3600)
