import scrapy


class NewsItem(scrapy.Item):
    url = scrapy.Field()
    source = scrapy.Field()
    capture_dt = scrapy.Field()
    other = scrapy.Field()
