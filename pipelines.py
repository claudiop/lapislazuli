import pymongo as pymongo

from mongo_utils import get_diff_dict, dict2dot


class MongoPipeline(object):
    collection_name = 'news_items'

    def __init__(self):
        pass

    @classmethod
    def from_crawler(cls, _crawler):
        return cls()

    def open_spider(self, _spider):
        self.client = pymongo.MongoClient('mongodb://%s:%s@127.0.0.1' % ('user', 'password')) # TODO, get this out of here
        self.db = self.client.news_items

    def close_spider(self, _spider):
        self.client.close()

    def process_item(self, item, spider):
        item_dict = dict(item)
        if 'other' in item_dict:
            other = item_dict.pop('other')
            for key, val in other.items():
                item_dict[key] = val

        document = self.db[spider.name].find_one({'url': item_dict['url']})
        if document is None:
            self.db[spider.name].insert_one(item_dict)
        else:
            if hasattr(spider, 'replace') and spider.replace:
                self.db[spider.name].replace_one({
                    '_id': document['_id']
                }, item_dict)
            else:
                new_document = {**document, **item_dict}
                self.db[spider.name].replace_one({
                    '_id': document['_id']
                }, new_document)

                # diff = get_diff_dict(dict(document), item_dict)
                # if len(diff):
                #     self.db[spider.name].update_one({
                #         '_id': document['_id']
                #     }, {
                #         '$set': dict2dot(diff)
                #     })
        return item
