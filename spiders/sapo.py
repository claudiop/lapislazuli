import locale
import re

import scrapy
from bs4 import BeautifulSoup
from datetime import datetime as dt
import htmlmin

from items import NewsItem

locale.setlocale(locale.LC_ALL, 'pt_PT.UTF-8')

MONTHLY_MAP_RE = re.compile(r'.*/sitemap_\w*_(\d{4})-(\d{1,2})\.xml')


class SapoSpider(scrapy.Spider):
    name = 'sapo'
    start_urls = ['https://sitemaps.sapo.pt/']

    # allowed_domains = ['sapo.pt'] TODO complete

    def __init__(self, min_date: dt, *args, **kwargs):
        super(SapoSpider, self).__init__(*args, **kwargs)
        self.min_date = min_date

    def parse(self, response):
        for url in response.xpath('//a/@href').getall():
            if '/projectmap/' in url and url.endswith('pt'):
                yield scrapy.Request(
                    response.urljoin(url),
                    self.parse_project_map,
                    priority=5)

    def parse_project_map(self, response):
        for url in response.xpath('//a/@href').getall():
            if url.endswith('.xml'):
                match = MONTHLY_MAP_RE.match(url)
                if match is None:
                    continue
                year, month = map(lambda i: int(i), match.groups())
                if year >= self.min_date.year and month >= self.min_date.month:
                    yield scrapy.Request(
                        response.urljoin(url),
                        self.parse_monthly_map,
                        priority=-1)

    def parse_monthly_map(self, response):
        soup = BeautifulSoup(response.body.decode(response.encoding), 'html.parser')
        for url_tag in soup.find_all("url"):
            url = url_tag.find_next("loc").text.strip()
            yield scrapy.Request(
                response.urljoin(url), self.parse_article, meta={'map_xml': htmlmin.minify(str(url_tag))},
                priority=2)

    def parse_article(self, response):
        yield NewsItem(
            url=response.url,
            source=htmlmin.minify(response.body.decode(response.encoding)),
            capture_dt=dt.now(),
            other={'map_xml': response.meta['map_xml']})
