import json
import locale
import logging
import htmlmin
import scrapy
from threading import Lock
from datetime import datetime as dt, timedelta, date
from bs4 import BeautifulSoup

from items import NewsItem

locale.setlocale(locale.LC_ALL, 'pt_PT.UTF-8')
log = logging.getLogger(__name__)


class ObservadorSpider(scrapy.Spider):
    name = 'observador'
    page = dt.now().date()
    start_urls = ['https://observador.pt/wp-json/obs_api/v4/grids/filter/homepage?offset=%d%02d%02d' % (
        page.year, page.month, page.day)]
    skipped_count = 0
    page_lock = Lock()

    def __init__(self, min_date: date, *args, **kwargs):
        super(ObservadorSpider, self).__init__(*args, **kwargs)
        self.min_date = min_date

    def parse(self, response):
        response_json = json.loads(response.body_as_unicode())
        if 'rendered' in response_json and 'modules' in response_json['rendered']:
            soup = BeautifulSoup(response_json['rendered']['modules'], "html.parser")
            for link in soup.find_all('a', href=True):
                href = link.get('href')
                if '/autor/' in href:
                    continue
                elif '/opiniao/' in href:
                    if '/autor/' not in href:
                        yield scrapy.Request(
                            response.urljoin(href),
                            self.noop_parse,
                            priority=2)
                elif '/especiais/' in href:
                    yield scrapy.Request(
                        response.urljoin(href),
                        self.noop_parse,
                        priority=2)
                elif '/programas/' in href:
                    slug = href.split('/programas/')[-1]
                    if slug.count('/') > 1:
                        yield scrapy.Request(
                            response.urljoin(href),
                            self.noop_parse,
                            priority=2)
                else:
                    parts = href.split('/')
                    if len(parts) < 5:
                        continue
                    year, month, day = parts[-5], parts[-4], parts[-3]
                    if len(year) == 4 and len(month) == len(day) == 2:
                        yield scrapy.Request(
                            response.urljoin(href),
                            self.noop_parse,
                            priority=2)
            self.page_lock.acquire()
            try:
                if self.min_date > self.page:
                    return
                self.skipped_count = 0
                yield scrapy.Request(
                    'https://observador.pt/wp-json/obs_api/v4/grids/filter/homepage?offset=%d%02d%02d'
                    % (self.page.year, self.page.month, self.page.day),
                    self.parse,
                    priority=-1)
                self.page -= timedelta(days=1)
            finally:
                self.page_lock.release()
        else:
            self.page_lock.acquire()
            try:
                if self.min_date > self.page:
                    return
                # Else, allow up to 10 continuous skips
                self.skipped_count += 1
                if self.skipped_count > 10:
                    log.warn("Objervador: Skips exceeded. Giving up.")
                    return
                self.page -= timedelta(days=1)
                yield scrapy.Request(
                    'https://observador.pt/wp-json/obs_api/v4/grids/filter/homepage?offset=%d%02d%02d'
                    % (self.page.year, self.page.month, self.page.day),
                    self.parse)
            finally:
                self.page_lock.release()
        log.info(f"Fetching page {self.page} of Observador")

    def noop_parse(self, response):
        yield NewsItem(
            url=response.url,
            source=htmlmin.minify(response.text),
            capture_dt=dt.now()
        )
