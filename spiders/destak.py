import htmlmin
import scrapy
from datetime import datetime as dt

from items import NewsItem


class DestakSpider(scrapy.Spider):
    name = 'destak'
    allowed_domains = ['destak.pt']

    def __init__(self, max_page: dt, *args, **kwargs):
        super(DestakSpider, self).__init__(*args, **kwargs)
        self.max_page = max_page

    def start_requests(self):
        for i in range(self.max_page):
            yield scrapy.Request('http://www.destak.pt/artigo/%i' % i, self.parse_article)

    def parse_article(self, response):
        item = NewsItem(
            url=response.url,
            source=htmlmin.minify(response.text),
            capture_dt=dt.now(),
        )
        yield item
