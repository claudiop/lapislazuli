import locale
import logging
import re

import scrapy
from bs4 import BeautifulSoup
from datetime import datetime as dt
from datetime import date
import htmlmin

from items import NewsItem

log = logging.getLogger(__name__)
locale.setlocale(locale.LC_ALL, 'pt_PT.UTF-8')

DAILY_MAP_RE = re.compile(r'.*/sitemap.xml\?yyyy=(\d{4})&mm=(\d{2})&dd=(\d{2})')


class DNJornalSpider(scrapy.Spider):
    name = 'dnjornal'
    start_urls = ['https://www.dn.pt/sitemap.xml']
    allowed_domains = ['www.dn.pt']

    def __init__(self, min_date: dt, *args, **kwargs):
        super(DNJornalSpider, self).__init__(*args, **kwargs)
        self.min_date = min_date

    def parse(self, response):
        soup = BeautifulSoup(response.body.decode(response.encoding), 'html.parser')
        loc_tags = soup.find_all("loc")
        for loc_tag in loc_tags:
            loc = loc_tag.text
            match = DAILY_MAP_RE.match(loc)
            loc_date = date(*map(lambda i: int(i), match.groups()))
            if loc_date >= self.min_date:
                yield scrapy.Request(loc, self.parse_daily_map, priority=-1)
                log.info("DN current date %s" % loc_date)

    def parse_daily_map(self, response):
        soup = BeautifulSoup(response.body.decode(response.encoding), 'html.parser')
        url_tags = soup.find_all("url")
        for url_tag in url_tags:
            loc = url_tag.find_next("loc").text.strip()
            yield scrapy.Request(loc, self.parse_article, meta={'map_xml': htmlmin.minify(str(url_tag))}, priority=1)

    def parse_article(self, response):
        yield NewsItem(
            url=response.url,
            source=htmlmin.minify(response.body.decode(response.encoding)),
            capture_dt=dt.now(),
            other={'map_xml': response.meta['map_xml']})
