import logging
import re

import htmlmin
import scrapy
from datetime import datetime as dt

from items import NewsItem

log = logging.getLogger(__name__)


class RtpSpider(scrapy.Spider):
    name = 'rtp'
    allowed_domains = ['rtp.pt']
    replace = True

    def __init__(self, min_id=0, max_id=26886178, known=None, proxy=None, *args, **kwargs):
        super(RtpSpider, self).__init__(*args, **kwargs)
        self.min_id = min_id
        self.max_id = max_id
        self.proxy = proxy
        self.known = {} if known is None else known

    def start_requests(self):
        skipped = 0
        for i in range(self.min_id, self.max_id):
            if i % 100 == 0:
                log.info("RTP Current: %i Skipped:%i " % (i, skipped))
                skipped = 0
            if i in self.known:
                skipped += 1
                continue

            yield scrapy.Request(
                'https://www.rtp.pt/noticias/article/content/%i' % i,
                self.parse_content_page,
                meta={'proxy': self.proxy, 'iid': i},
                priority=-1)

    def parse_content_page(self, response):
        iid = response.meta['iid']
        canonical_url = response.xpath('//link[@itemprop="mainEntityOfPage"]')
        if canonical_url is None or 'href' not in canonical_url.attrib:
            log.warn("RTP Skipped %s" % iid)
            return
        canonical_url = canonical_url.attrib['href'].strip().replace('//www.', 'https://www.')
        if canonical_url == "":
            log.warn("RTP Skipped %s (no canonical)" % iid)
            return
        else:
            yield scrapy.Request(
                canonical_url,
                self.noop_parse,
                meta={'iid': iid, 'proxy': self.proxy})

    def parse_news(self, response):
        iid = response.meta['iid']
        canonical_url = response.xpath('//link[@rel="canonical"]')
        if canonical_url is None or 'href' not in canonical_url.attrib:
            log.warn("RTP Skipped %s" % iid)
            return
        canonical_url = canonical_url.attrib['href'].strip()
        if canonical_url == "":
            log.warn("RTP Skipped %s (no canonical)" % iid)
            return
        if '_n' in canonical_url and response.css('.sizearticle').get():
            yield NewsItem(
                url=canonical_url,
                source=htmlmin.minify(response.text),
                capture_dt=dt.now(),
                other={'iid': iid}
            )
        else:
            yield scrapy.Request(
                canonical_url,
                self.noop_parse,
                meta={'iid': iid, 'proxy': self.proxy})

    def parse_home_feed(self, response):
        if response.url == "https://www.rtp.pt/404/index.php":
            log.warn("RTP 404 on %s" % response.meta['iid'])
            return

        exp = re.compile('.*rtp\.pt/noticias/.+_\w(?P<id>\d+)')
        skips = 0
        for url in response.xpath('//a/@href').getall():
            match = exp.match(url)
            if match is not None:
                iid = match.group("id")
                if iid not in self.known:
                    yield scrapy.Request(
                        url,
                        self.noop_parse,
                        meta={'iid': iid, 'proxy': self.proxy})
                else:
                    skips += 1
        if skips > 0:
            log.warn("RTP Skip #%i in %s" % (skips, response.url))

    def noop_parse(self, response):
        if response.url == "https://www.rtp.pt/404/index.php":
            log.warn("RTP 404 on %s" % response.meta['iid'])
            return
        yield NewsItem(
            url=response.url,
            source=htmlmin.minify(response.text),
            capture_dt=dt.now(),
            other={'iid': response.meta['iid']}
        )
