import locale
import re

import scrapy
from bs4 import BeautifulSoup
from datetime import datetime as dt
from datetime import date
import htmlmin

from items import NewsItem

locale.setlocale(locale.LC_ALL, 'pt_PT.UTF-8')

MONTHLY_MAP_RE = re.compile(r'.*/sitemap/expresso_(\d{4})_(\d{1,2})\.xml')


class ExpressoSpider(scrapy.Spider):
    name = 'expresso'
    start_urls = ['https://expresso.pt/sitemap/expresso_index.xml']

    # allowed_domains = ['expresso.pt', 'tribunaexpresso.pt']

    def __init__(self, min_date: dt, *args, **kwargs):
        super(ExpressoSpider, self).__init__(*args, **kwargs)
        self.min_date = min_date

    def parse(self, response):
        soup = BeautifulSoup(response.body.decode(response.encoding), 'html.parser')
        loc_tags = soup.find_all("loc")
        for loc_tag in loc_tags:
            loc = loc_tag.text
            match = MONTHLY_MAP_RE.match(loc)
            loc_date = date(*map(lambda i: int(i), match.groups()), day=1)
            if loc_date.year >= self.min_date.year and loc_date.month >= self.min_date.month:
                yield scrapy.Request(loc, self.parse_monthly_map)

    def parse_monthly_map(self, response):
        soup = BeautifulSoup(response.body.decode(response.encoding), 'html.parser')
        url_tags = soup.find_all("url")
        for url_tag in url_tags:
            loc = url_tag.find_next("loc").text.strip()

            yield scrapy.Request(
                loc,
                self.parse_article,
                meta={'map_xml': htmlmin.minify(str(url_tag))})

    def parse_article(self, response):
        map_xml = response.meta['map_xml']
        item = NewsItem(
            url=response.url,
            source=htmlmin.minify(response.body.decode(response.encoding)),
            capture_dt=dt.now(),
            other={'map_xml': map_xml})
        yield item
