import locale
import logging
import re

import scrapy
from bs4 import BeautifulSoup
from datetime import datetime as dt
from datetime import date
import htmlmin

from items import NewsItem

log = logging.getLogger(__name__)
locale.setlocale(locale.LC_ALL, 'pt_PT.UTF-8')

DAILY_MAP_RE = re.compile(r'.*/sitemap\?yyyy=(\d{4})&mm=(\d{2})&dd=(\d{2})')


class CMJornalSpider(scrapy.Spider):
    name = 'cmjornal'
    start_urls = ['https://www.cmjornal.pt/sitemap']

    def __init__(self, min_date: dt, *args, **kwargs):
        super(CMJornalSpider, self).__init__(*args, **kwargs)
        self.min_date = min_date

    def parse(self, response):

        soup = BeautifulSoup(response.body.decode(response.encoding), 'html.parser')
        sitemap_tags = soup.find_all("sitemap")
        for sitemap in sitemap_tags:
            loc = sitemap.find_next("loc").text
            match = DAILY_MAP_RE.match(loc)
            loc_date = date(*map(lambda i: int(i), match.groups()))
            if loc_date >= self.min_date:
                yield scrapy.Request(loc, self.parse_daily_map, meta={'date': loc_date})

    def parse_daily_map(self, response):
        soup = BeautifulSoup(response.body.decode(response.encoding), 'html.parser')
        url_tags = soup.find_all("url")
        for article in url_tags:
            title = article.find_next("news:title").text.strip()
            loc = article.find_next("loc").text.strip()
            lastmod = dt.strptime(
                article.find_next("lastmod").text.strip(),
                '%Y-%m-%dT%H:%M:%S%z')
            publication = dt.strptime(
                article.find_next("news:publication_date").text.strip(),
                '%Y-%m-%dT%H:%M:%S%z')
            thumbnail = article.find_next("image:loc")
            keywords = list(map(
                lambda keyword: keyword.strip(),
                article.find_next("news:keywords").text.split(',')))

            meta = {
                'publication': publication,
                'modification': lastmod,
                'title': title,
                'map_xml': htmlmin.minify(str(article)),
                'url': loc
            }
            if keywords != ['']:
                meta['keywords'] = keywords
            if thumbnail:
                meta['thumbnail'] = thumbnail.text.strip()
            yield scrapy.Request(
                loc,
                self.parse_article,
                meta={'other': meta})  # To avoid getting mixed with stats

    def parse_article(self, response):
        source = htmlmin.minify(response.body.decode(response.encoding))

        meta = response.meta['other']
        url = meta.pop("url")
        other = {
            'title': meta.pop("title"),
            'publish_datetime': meta.pop("publication"),
            'slug': url.split('/detalhe/')[-1],
            'map_xml': meta['map_xml']}
        if 'keywords' in meta:
            other['keywords'] = meta['keywords']
        if 'thumbnail' in meta:
            other['thumbnail'] = meta['thumbnail']
        yield NewsItem(
            url=url,
            source=source,
            capture_dt=dt.now(),
            other=other)
