import json
import logging
import threading
from threading import Lock
from time import sleep

import htmlmin
import scrapy
from datetime import datetime as dt

from items import NewsItem

log = logging.getLogger(__name__)


class PublicoSpider(scrapy.Spider):
    name = 'publico'
    replace = True

    def __init__(self, min_date=dt.now(), max_page=100, *args, **kwargs):
        super(PublicoSpider, self).__init__(*args, **kwargs)
        self.min_date = min_date
        self._lock = threading.Lock()
        self.oldest_date_found = dt.now()
        self.max_page = max_page

    def start_requests(self):
        for i in range(self.max_page):
            lock = threading.Lock()
            lock.acquire()
            yield scrapy.Request(
                'https://www.publico.pt/api/list/ultimas?page=%i' % i,
                self.parse,
                priority=-1)
            # self._lock.acquire()
            # try:
            #     print("Tick")
            #     print(self.oldest_date_found)
            #     if self.min_date > self.oldest_date_found:
            #         print("Baaam")
            #         break
            # finally:
            #     self._lock.release()
            if i % 50 == 0:
                log.info("Público: %i" % i)

    def parse(self, response):
        try:
            response_json = json.loads(response.body_as_unicode())
        except json.decoder.JSONDecodeError:
            print("Failed to decode: %s" % response.body_as_unicode())
            return

        for item_json in response_json:
            # try:
            #     title = BeautifulSoup(item_json['tituloNoticia'], "html.parser").text
            # except TypeError:
            #     continue  # Possibly redacted
            #
            datetime = dt.strptime(item_json['data'], '%Y-%m-%dT%H:%M:%S')
            self._lock.acquire()
            try:
                if self.oldest_date_found > datetime:
                    self.oldest_date_found = datetime
                # if self.min_date > datetime:
                #     return
            finally:
                self._lock.release()

            url = item_json['shareUrl']
            yield scrapy.Request(
                url,
                self.fetch_source,
                priority=2,
                meta={'api_json': item_json})

    def fetch_source(self, response):
        yield NewsItem(
            url=response.url,
            source=htmlmin.minify(response.body.decode(response.encoding)),
            capture_dt=dt.now(),
            other={
                'api_json': response.meta['api_json']
            }
        )
