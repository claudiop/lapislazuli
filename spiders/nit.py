import json
import locale
import logging
import re
from threading import Lock

import htmlmin
import scrapy
from datetime import datetime as dt
from bs4 import BeautifulSoup

from items import NewsItem


log = logging.getLogger(__name__)
locale.setlocale(locale.LC_ALL, 'pt_PT.UTF-8')


class NitSpider(scrapy.Spider):
    name = 'nit'
    page = 0
    page_lock = Lock()
    allowed_domains = ['nit.pt']

    def __init__(self, nounce, quick=False, *args, **kwargs):
        super(NitSpider, self).__init__(*args, **kwargs)
        self.quick = quick
        self.nounce = nounce

    def start_requests(self):
        if self.quick:
            yield scrapy.Request('https://nit.pt/', self.parse_home)
        else:
            for i in range(self.page):
                yield scrapy.Request(
                    'https://nit.pt/wp-admin/admin-ajax.php'
                    '?action=alm_query_posts&nonce=%s'
                    '&posts_per_page=1000&page=%d' % (self.nounce, i),
                    self.parse_ajax)

    def parse_home(self, response):
        exp = re.compile('.*nit\.pt/\S+/.+')
        for url in response.xpath('//a/@href').getall():
            if '?' in url:
                url = url.split('?')[0]
            if exp.match(url):
                yield scrapy.Request(
                    response.urljoin(url),
                    self.noop_parse)

    def parse_ajax(self, response):
        response_json = json.loads(response.body_as_unicode())
        if 'meta' in response_json and 'postcount' in response_json['meta']:
            if response_json['meta']['postcount'] == 0:
                return
            soup = BeautifulSoup(response_json['html'], 'html.parser')
            for link in soup.find_all('a'):
                yield scrapy.Request(link.attrs['href'], self.noop_parse, priority=2)

            self.page_lock.acquire()
            try:
                self.page += 1
                yield scrapy.Request(
                    'https://nit.pt/wp-admin/admin-ajax.php'
                    '?action=alm_query_posts&nonce=%s'
                    '&posts_per_page=1000&page=%d'
                    % (self.nounce, self.page),
                    self.parse_ajax,
                    priority=-1)
                log.info(f"Nit: {self.page}")
            finally:
                self.page_lock.release()

    def noop_parse(self, response):
        yield NewsItem(
            url=response.url,
            source=htmlmin.minify(response.text),
            capture_dt=dt.now()
        )
