import locale
import logging
import re
import scrapy
import htmlmin
from datetime import datetime as dt
from items import NewsItem

locale.setlocale(locale.LC_ALL, 'pt_PT.UTF-8')
LUSA_URL_RE = re.compile(".*lusa.pt/(?P<page>\\w*)/.*$")

log = logging.getLogger(__name__)


class LusaSpider(scrapy.Spider):
    name = 'lusa'
    allowed_domains = ['lusa.pt']

    def __init__(self, known_set=None, min_id=0, max_id=26886178, proxy=None, retry=False, *args, **kwargs):
        super(LusaSpider, self).__init__(*args, **kwargs)

        self.min_id = min_id
        self.max_id = max_id
        self.retry = retry
        self.known = {} if known_set is None else known_set
        self.proxy = proxy

    def start_requests(self):
        for i in range(self.min_id, self.max_id):
            if i in self.known:
                continue
            meta = {'iid': i} if self.proxy is None else {'iid': i, 'proxy': self.proxy}
            yield scrapy.Request(
                'https://www.lusa.pt/article/%i/' % i,
                self.parse,
                meta=meta)
            if i % 10000 == 0:
                log.info(f"Lusa generated page id {i}")

    def parse(self, response):
        article_elem = response.css('.article')
        iid = response.meta['iid']
        title = article_elem.css('h2::text').get()
        if title is not None and title.strip().endswith('not found.'):
            logging.debug("Skipped #%i" % iid)
            return
        if iid % 200 == 0:
            log.info(f"Lusa on page id {iid}")
        source = htmlmin.minify(response.body.decode(response.encoding))
        if self.retry and source.strip() == "":
            meta = {'iid': iid} if self.proxy is None else {'iid': iid, 'proxy': self.proxy}
            yield scrapy.Request(
                response.url,
                self.parse,
                dont_filter=True,
                priority=100,
                meta=meta)
        else:
            yield NewsItem(
                url=response.url,
                source=source,
                capture_dt=dt.now(),
                other={'iid': response.meta['iid']}
            )
