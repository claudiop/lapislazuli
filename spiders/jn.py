import locale
import logging
import re
from threading import Lock

import scrapy
from datetime import datetime as dt
import htmlmin

from items import NewsItem
from scrapy.linkextractors import LinkExtractor

log = logging.getLogger(__name__)
locale.setlocale(locale.LC_ALL, 'pt_PT.UTF-8')

DAILY_MAP_RE = re.compile(r'.*/sitemap.xml\?yyyy=(\d{4})&mm=(\d{2})&dd=(\d{2})')


class JNSpider(scrapy.Spider):
    name = 'jn'
    start_urls = ['https://www.jn.pt/ajax/requests.aspx']
    allowed_domains = ['jn.pt']

    def __init__(self, cookie, max_page=100, *args, **kwargs):
        super(JNSpider, self).__init__(*args, **kwargs)
        self.cookie = cookie
        self.page = 0
        self.max_page = max_page
        self.page_lock = Lock()

    def start_requests(self):
        yield scrapy.FormRequest(
            'https://www.jn.pt/ajax/requests.aspx',
            self.parse_request,
            formdata={
                'op': 'LoadMoreSection',
                'PathName': '/NewsGen/Edição/JN/Nacional|/NewsGen/Edição/JN/Justiça|/NewsGen/Edição/JN/Economia|'
                            '/NewsGen/Edição/JN/País/Notícias|/NewsGen/Edição/JN/Mundo|/NewsGen/Edição/JN/Desporto|'
                            '/NewsGen/Edição/JN/Cultura|/NewsGen/Edição/JN/Gente|/NewsGen/Edição/JN/Tecnologia',
                'PageNumber': '3090'
            },
            headers={
                'Origin': 'https://www.jn.pt',
                'Referer': 'https://www.jn.pt/ultimas.html',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                'Cookie': self.cookie
            })

    def parse_request(self, response):
        extractor = LinkExtractor(allow='.*\.html', deny='.*/tag/.*')
        links = extractor.extract_links(response)
        for link in links:
            yield scrapy.Request(link.url, self.parse_article, priority=2)
        if len(links) > 0:
            self.page_lock.acquire()
            try:
                log.info(f"JN: {self.page}")
                self.page += 1
                if self.page > self.max_page:
                    return
                yield scrapy.FormRequest(
                    'https://www.jn.pt/ajax/requests.aspx',
                    self.parse_request,
                    formdata={
                        'op': 'LoadMoreSection',
                        'PathName': '/NewsGen/Edição/JN/Nacional|/NewsGen/Edição/JN/Justiça|/NewsGen/Edição/JN'
                                    '/Economia|/NewsGen/Edição/JN/País/Notícias|/NewsGen/Edição/JN/Mundo|/NewsGen'
                                    '/Edição/JN/Desporto|/NewsGen/Edição/JN/Cultura|/NewsGen/Edição/JN/Gente|'
                                    '/NewsGen/Edição/JN/Tecnologia',
                        'PageNumber': str(self.page)
                    },
                    headers={
                        'Origin': 'https://www.jn.pt',
                        'Referer': 'https://www.jn.pt/ultimas.html',
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                        'Cookie': self.cookie
                    },
                    priority=-1)
            finally:
                self.page_lock.release()

    def parse_article(self, response):
        yield NewsItem(
            url=response.url,
            source=htmlmin.minify(response.body.decode(response.encoding)),
            capture_dt=dt.now())
