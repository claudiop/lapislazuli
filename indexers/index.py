import json
import logging
from queue import Queue

import pymongo

from indexers.fetcher import Fetcher
from indexers.indexer import Indexer
from indexers.parsers.cmjornal import CMJornalParser
from indexers.parsers.lusa import LusaParser
from indexers.parsers.publico import PublicoParser
from indexers.pipelines.elastic import ElasticPipeline
from indexers.pipelines.postgres import PostgreSQLPipeline

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


config = json.load(open('../config.json'))

mongo = pymongo.MongoClient('mongodb://%s:%s@127.0.0.1' % (config['mongo_user'], config['mongo_password']))
db = mongo.news_items

output = Queue(maxsize=2)
fetchers = [
    # Fetcher(db["lusa"], LusaParser(), output),
    # Fetcher(db["cmjornal"], CMJornalParser(), output),
    Fetcher(db["publico"], PublicoParser(), output),
]
for fetcher in fetchers:
    fetcher.start()

# ElasticPipeline(config),
pipeline = [PostgreSQLPipeline(config)]
indexers = []
for _ in range(config['indexer_count']):
    indexers.append(Indexer(output, config, pipeline))
for indexer in indexers:
    indexer.start()

while True:
    inp = input(">")
    if 'exit' in inp:
        print("Exiting")
        for fetcher in fetchers:
            fetcher.stop = True
        for indexers in indexers:
            indexers.stop = True
            indexers.join()
        for fetcher in fetchers:
            fetcher.join()
        break
