import json

import datetime as dt
from bs4 import BeautifulSoup

from entities import publico, lusa, reuters
from indexers.utils import minify_url
from semantic.schemas.creative_work import NewsArticle
from semantic.schemas.media import ImageObject
from semantic.schemas.person import Person
from semantic import types


class PublicoParser:
    meta_version = 0
    ignored_authors = {'Culto', 'Fugas'}

    def parse_document(self, original_doc):
        soup = BeautifulSoup(original_doc['source'], features="html.parser")

        url = minify_url(original_doc['url'])
        if url.endswith('/noticia/'):
            return None

        # Data returned from the API, if present
        api_json = original_doc['api_json'] if 'api_json' in original_doc else None

        # Try to parse the embedded JSON meta-field
        # This is susceptible to failure as the folks at Público didn't knew how to escape strings
        meta_json_tag = soup.find_all('script', {"type": "application/ld+json"})
        if len(meta_json_tag) == 0:
            embedded_json = None
        elif len(meta_json_tag) == 1:
            embedded_json = self.parse_json(meta_json_tag[0].get_text())
        else:
            embedded_json = {}
            for tag in meta_json_tag:
                to_append = self.parse_json(tag.get_text())
                if to_append is not None:
                    to_append = dict()
                    for key, val in to_append.items():
                        if val.strip() == "":
                            del to_append[key]
                    embedded_json = {**embedded_json, **to_append}
            if embedded_json == {}:
                embedded_json = None

        # Fields that are going to get parsed with the first source that works
        images = set()
        type_str = None
        layout = None
        was_updated = False
        description = None
        authors = set()
        word_count = None
        keywords = set()
        creation = None
        publication = None
        modification = None
        thumbnailUrl = None
        # Deduplicate
        contributors = dict()

        # If the API data is present extract the fields that are know to be good from there
        if api_json is not None:
            article = NewsArticle()
            headline = api_json['cleanTitle']
            description = api_json['descricao']
            thumbnailUrl = api_json['multimediaPrincipal']
            # images.append((api_json['multimediaPrincipalLegenda'], api_json['multimediaPrincipal']))
            type_str = api_json['tipo']
            layout = api_json['tipoLayout']
            for author_meta in api_json['autores']:
                author_name = author_meta['nome']
                if author_name == "PÚBLICO":
                    continue
                elif author_name.lower() in ("lusa", 'agência lusa'):
                    authors.add(lusa)
                    continue
                elif author_name == "Lusa e PÚBLICO":
                    authors.add(publico)
                    authors.add(lusa)
                    continue
                elif author_name == "Reuters":
                    authors.add(reuters)
                    continue
                elif author_name in self.ignored_authors:
                    continue

                if author_name in contributors:
                    author = contributors[author_name]
                    authors.add(author)
                else:
                    author = Person()
                    if author_meta['tipo'] == "EXTERNO":
                        continue
                    author.add("name", types.Text(author_meta["nome"]))
                    if author_meta['email'] is not None and author_meta['email'] != "":
                        author.add("email", types.Text(author_meta["email"]))
                    if author_meta['url'] is None:
                        if author_meta["slug"] is not None and author_meta["slug"] != "":
                            author.add("url", types.URL("https://www.publico.pt/autor/" + author_meta["slug"]))
                    else:
                        author.add("url", types.URL("https://www.publico.pt" + author_meta["url"]))
                    contributors[author_name] = author
                    authors.add(author)

            for image_meta in api_json['imagens']:
                author_name = image_meta['autor']
                image = ImageObject()
                if author_name in contributors:
                    image_author = contributors[author_name]
                    contributors[author_name] = image_author
                elif image_meta['nome'] is not None:
                    image_author = Person()
                    image_author.add("name", types.Text(image_meta['nome']))
                    image_author = Person()
                    image_author.add("worksFor", publico)
                    image.add("author", image_author)
                image.add("url", types.URL(image_meta['url']))
                if image_meta['largura'] is not None:
                    image.add("width", types.Number(int(image_meta['largura'])))
                if image_meta['altura'] is not None:
                    image.add("height", types.Number(int(image_meta['altura'])))
                if image_meta['legenda'] is not None:
                    image.add("caption", types.Text(image_meta['legenda']))
                if image_meta['tags'] is not None:
                    image.add("keywords", types.Text(image_meta['tags']))
                images.add(image)

            was_updated = api_json['dataActualizacao'] is None
            try:
                creation = dt.datetime.strptime(
                    api_json['data'],
                    "%a, %d %b %Y %H:%M:%S %Z")
                if was_updated:
                    modification = creation
                else:
                    modification = dt.datetime.strptime(
                        api_json['dataActualizacao'],
                        "%a, %d %b %Y %H:%M:%S %Z")
            except (KeyError, ValueError):
                pass
            word_count = api_json['wordCount']
            # is_opinion = 'opinion' in original_doc and original_doc['opinion']
            # if is_opinion:
            #     article = OpinionArticle() TODO
            # is_sponsored = 'sponsored' in original_doc and original_doc['sponsored']

        # If there is no API data use the embedded metadata
        elif embedded_json is not None:
            article = NewsArticle()
            headline = embedded_json['headline']
            thumbnailUrl = embedded_json['thumbnailUrl']
            keywords = embedded_json['keywords']

            image_meta = embedded_json['image']
            image = ImageObject()
            image.add("url", types.URL(image_meta['url']))
            image.add("height", types.Number(int(image_meta['height'])))
            image.add("height", types.Number(int(image_meta['height'])))
            image.add("caption", types.Text(image_meta['legenda']))
            images.add(image)

        # If everything failed, play a guessing game
        else:
            article = NewsArticle()
            headline = ''  # TODO
            image_tag = soup.find('meta', {"property": "og:image"})
            if image_tag:
                images = [image_tag.attrs['content'], ]
            del image_tag

            keyword_tag = soup.find('meta', {"name": "keywords"})
            if keyword_tag:  # Bad
                keywords = list(map(
                    lambda keyword: keyword.strip(),
                    keyword_tag
                        .attrs['content']
                        .split(',')))
            else:  # Worse
                for tag in soup.find(class_='menu--tag').descendants:
                    if hasattr(tag, 'get_text'):
                        keyword = tag.get_text().strip()
                        if keyword != '':
                            keywords.add(keyword)
            del keyword_tag

        if len(authors) == 0:
            author_tag = soup.find('meta', {"name": "author"})
            if author_tag:
                author_str_list = list(map(
                    lambda author: author.strip(),
                    soup.find('meta', {"name": "author"})
                        .attrs['content']
                        .split(',')))
            # TODO use this
            del author_tag

        # Extract the body from the page.
        body_tag = soup.find('div', id='story-body')
        if body_tag is None:
            body_tag = soup.find('div', class_='entry-body')
        if body_tag is None:
            return None
        [elem.decompose() for elem in body_tag.find_all('script')]
        [elem.decompose() for elem in body_tag.find_all('aside')]
        [elem.decompose() for elem in body_tag.find_all(class_='supplemental-slot')]
        expand_btn = body_tag.find(class_='story__show-full')
        if expand_btn:
            expand_btn.decompose()
        del expand_btn
        content = ' '.join(body_tag.get_text('\n').split()).replace(' ,', ',')
        del body_tag

        # If dates weren't found in the API, find them in the embedded metadata
        if creation is None:
            try:
                creation = dt.datetime.strptime(
                    embedded_json['dateCreated'],
                    "%a, %d %b %Y %H:%M:%S %Z")
            except (KeyError, ValueError, TypeError):
                return None

        if publication is None or publication == creation:
            try:
                if 'datePublished' in embedded_json:
                    publication = dt.datetime.strptime(
                        embedded_json['datePublished'],
                        "%a, %d %b %Y %H:%M:%S %Z")
                else:
                    publication = creation
            except (KeyError, ValueError):
                pass

        if modification is None or modification == creation:
            try:
                if 'dateModified' in embedded_json:
                    modification = dt.datetime.strptime(
                        embedded_json['dateModified'],
                        "%a, %d %b %Y %H:%M:%S %Z")
                else:
                    modification = creation
            except (KeyError, ValueError):
                pass

        # The dates aren't in the metadata. Fingers crossed & HTML extraction
        if creation is None:
            time_tag = soup.find('time', class_="dateline")
            if time_tag is None:
                print("Unable to find time in: %s", url)
                return None
            else:
                publication = dt.datetime.strptime(
                    time_tag.attrs['datetime'],
                    "%a, %d %b %Y %H:%M:%S %Z")

        # Build the article
        article.add("url", types.URL(url))
        article.add("headline", types.Text(headline))
        article.add("articleBody", types.Text(content))
        article.add("publisher", publico)
        for keyword in keywords:
            article.add("keyword", types.Text(keyword))
        for author in authors:
            article.add("author", author)
        for image in images:
            article.add("image", image)
        if thumbnailUrl is not None:
            article.add("thumbnailUrl", types.URL(thumbnailUrl))
        article.add("dateCreated", types.DateTime(creation))
        if publication is not None:
            article.add("datePublished", types.Date(publication.date()))
        if modification is not None:
            article.add("dateModified", types.DateTime(modification))
        if description is not None:
            article.add("description", types.Text(description))
        if word_count is not None:
            article.add("wordCount", types.Number(word_count))
        return article

    @staticmethod
    def parse_json(meta_json):
        try:
            return json.loads(meta_json.replace('\\', ','))
        except json.decoder.JSONDecodeError:
            try:
                return json.loads(meta_json.replace('\\', ',').replace('},}', '}}'))
            except json.decoder.JSONDecodeError:
                return None
