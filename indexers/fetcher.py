import logging
import threading
from queue import Queue

log = logging.getLogger(__name__)


class Fetcher(threading.Thread):

    def __init__(self, collection, parser, output: Queue):
        super().__init__()
        self.collection = collection
        self.stop = False
        self.count = 0
        self.skips = 0
        self.output = output
        self.parser = parser
        self.filter = {"$and": [
            {"$or": [
                {"upstream": {"$exists": False}},
                {"upstream": {"$lt": parser.meta_version}}
            ]},
            {"source": {"$exists": True}},
            # {"$or": [
            #     {"failed": {"$exists": False}},
            #     {"failed": {"$lt": meta_version}}
            # ]}
        ]}

    def run(self):
        unindexed = self.collection.find(self.filter)
        for data in unindexed:
            try:
                document = self.parser.parse_document(data)
            except Exception as e:
                log.error(f"Failed on {data['url']}")
                document = None
            if document is not None:
                self.output.put(document)
                self.collection.update_one({
                    '_id': data['_id']
                }, {
                    '$set': {'upstream': self.parser.meta_version},
                    '$unset': {'failed': ""},
                }, upsert=False)
            else:
                pass
                self.collection.update_one({
                    '_id': data['_id']
                }, {
                    '$set': {'failed': self.parser.meta_version}
                })
                self.skips += 1
                if self.skips % 500 == 0:
                    print("Skipped %s" % self.skips)
            if self.stop:
                break
