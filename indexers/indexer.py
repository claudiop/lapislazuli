from queue import Queue
from threading import Thread


class Indexer(Thread):
    def __init__(self, input: Queue, settings, pipeline):
        super().__init__()
        self.input = input
        self.pipeline = pipeline
        self.settings = settings
        self.stop = False

    def run(self) -> None:
        while not self.stop:
            try:
                item = self.input.get(timeout=5000)
                for destination in self.pipeline:
                    try:
                        item = destination.process_item(item)
                        if item is None:
                            break
                    except Exception as e:
                        pass
            except TimeoutError:
                # Bypass to check if there is a stop request
                pass
