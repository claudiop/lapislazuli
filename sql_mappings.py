from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey, DateTime, Date, Table, Boolean
from sqlalchemy.orm import relationship, backref

Base = declarative_base()


class Thing(Base):
    __tablename__ = 'things'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    description = Column(String)
    url = Column(String)
    image = Column(String)
    type = Column(String(50))

    def __repr__(self):
        return "<Thing(name='%s', url='%s')>" % (
            self.name, self.url)

    __mapper_args__ = {
        'polymorphic_identity': 'things',
        'polymorphic_on': type
    }


organization_works_for_persons = Table(
    'organization_works_for_persons', Base.metadata,
    Column('organization_id', ForeignKey('organizations.id'), primary_key=True),
    Column('person_id', ForeignKey('persons.id'), primary_key=True)
)


class Organization(Thing):
    __tablename__ = 'organizations'

    id = Column(Integer, ForeignKey('things.id'), primary_key=True)
    emails = relationship('OrganizationEmail')
    telephones = relationship('OrganizationTelephone')
    addresses = relationship('OrganizationAddress')
    workers = relationship(
        'Person',
        secondary=organization_works_for_persons,
        back_populates='worksFor')
    works = relationship(
        'CreativeWork',
        secondary='creative_work_authors_org',
        back_populates='authorsOrg')

    # workers = relationship(
    #     'Person',
    #     secondary=organization_works_for_persons,
    #     primaryjoin=Thing.id == organization_works_for_persons.c.organization_id,
    #     secondaryjoin=Thing.id == organization_works_for_persons.c.person_id,
    #     backref=backref('worksFor'))

    def __repr__(self):
        return "<Organization(name='%s', url='%s')>" % (
            self.name, self.url)

    def __eq__(self, other):
        if not isinstance(other, Organization):
            return False
        return self.name == other.name

    __mapper_args__ = {
        'polymorphic_identity': 'organizations',
    }


class OrganizationEmail(Base):
    __tablename__ = 'organization_emails'

    id = Column(Integer, primary_key=True)
    email = Column(String, unique=True)
    organization = Column(ForeignKey(Organization.id))


class OrganizationAddress(Base):
    __tablename__ = 'organization_addresses'

    id = Column(Integer, primary_key=True)
    address = Column(String, unique=True)
    organization = Column(ForeignKey(Organization.id))


class OrganizationTelephone(Base):
    __tablename__ = 'organization_telephones'

    id = Column(Integer, primary_key=True)
    telephone = Column(String, unique=True)
    organization = Column(ForeignKey(Organization.id))


class Person(Thing):
    __tablename__ = 'persons'

    id = Column(Integer, ForeignKey('things.id'), primary_key=True)
    schema = Column(String)
    givenName = Column(String)
    additionalName = Column(String)
    familyName = Column(String)
    knowsLanguage = Column(String)
    emails = relationship('PersonEmail')
    telephones = relationship('PersonTelephone')
    worksFor = relationship(
        'Organization',
        secondary=organization_works_for_persons,
        # primaryjoin=Thing.id == organization_works_for_persons.c.organization_id,
        # secondaryjoin=Thing.id == organization_works_for_persons.c.person_id,
        # backref=backref('workers'))
    )
    works = relationship(
        'CreativeWork',
        secondary='creative_work_authors_per',
        back_populates='authorsPer')

    def __repr__(self):
        return "<Person(name='%s', url='%s')>" % (
            self.name, self.url)

    __mapper_args__ = {
        'polymorphic_identity': 'persons',
    }


class PersonEmail(Base):
    __tablename__ = 'person_emails'

    id = Column(Integer, primary_key=True)
    email = Column(String, unique=True)
    person = Column(ForeignKey(Person.id))


class PersonTelephone(Base):
    __tablename__ = 'person_telephones'

    id = Column(Integer, primary_key=True)
    telephone = Column(String, unique=True)
    person = Column(ForeignKey(Person.id))


creative_work_keywords = Table(
    'creative_work_keywords', Base.metadata,
    Column('creative_work_id', ForeignKey('creative_works.id'), primary_key=True),
    Column('keyword_id', ForeignKey('keywords.id'), primary_key=True)
)


class Keyword(Base):
    __tablename__ = 'keywords'

    id = Column(Integer, primary_key=True)
    text = Column(String, unique=True)
    creative_works = relationship(
        'CreativeWork',
        secondary='creative_work_keywords',
        back_populates='keywords')


creative_work_mentions = Table(
    'creative_work_mentions', Base.metadata,
    Column('creative_work_id', ForeignKey('creative_works.id'), primary_key=True),
    Column('mentioned_thing_id', ForeignKey('things.id'), primary_key=True)
)

creative_work_authors_org = Table(
    'creative_work_authors_org', Base.metadata,
    Column('creative_work_id', ForeignKey('creative_works.id'), primary_key=True),
    Column('organization_id', ForeignKey('organizations.id'), primary_key=True)
)

creative_work_authors_per = Table(
    'creative_work_authors_per', Base.metadata,
    Column('creative_work_id', ForeignKey('creative_works.id'), primary_key=True),
    Column('person_id', ForeignKey('persons.id'), primary_key=True)
)


class CreativeWork(Thing):
    __tablename__ = 'creative_works'

    id = Column(Integer, ForeignKey('things.id'), primary_key=True)
    alternativeHeadline = Column(String)
    # author = Column(ForeignKey('persons.id'))
    copyrightHolderOrg = Column(ForeignKey('organizations.id'))
    copyrightHolderPer = Column(ForeignKey('persons.id'))
    copyrightYear = Column(Integer)
    dateCreated = Column(DateTime)
    dateModified = Column(DateTime)
    datePublished = Column(Date)
    headline = Column(String)
    isAccessibleForFree = Column(String)
    isBasedOn = Column(ForeignKey('creative_works.id'))
    authorsOrg = relationship(
        'Organization',
        secondary='creative_work_authors_org',
        back_populates='works')
    authorsPer = relationship(
        'Person',
        secondary='creative_work_authors_per',
        back_populates='works')
    keywords = relationship(
        'Keyword',
        secondary='creative_work_keywords',
        back_populates='creative_works')
    mentions = relationship(
        'Thing',
        secondary=creative_work_mentions,
        primaryjoin=Thing.id == creative_work_mentions.c.creative_work_id,
        secondaryjoin=Thing.id == creative_work_mentions.c.mentioned_thing_id,
        backref=backref('mentioned_by'))
    # mentions = relationship(
    #     'CreativeWork',
    #     secondary='creative_work_mentions',
    #     back_populates='mentions')
    provider = Column(ForeignKey('organizations.id'))
    publisher = Column(ForeignKey('organizations.id'))
    thumbnailUrl = Column(String)

    def __repr__(self):
        return "<CreativeWork(headline='%s', url='%s')>" % (
            self.headline, self.url)

    __mapper_args__ = {
        'polymorphic_identity': 'creative_works',
    }


class MediaObject(CreativeWork):
    __tablename__ = 'media_objects'

    id = Column(Integer, ForeignKey('creative_works.id'), primary_key=True)
    parent = relationship("CreativeWork", foreign_keys=[id], backref='media_objects')
    bitrate = Column(Integer)
    contentSize = Column(String)
    contentUrl = Column(String)
    embedUrl = Column(String)
    encodesCreativeWork_id = Column(Integer, ForeignKey('creative_works.id'))
    encodesCreativeWork = relationship("CreativeWork", foreign_keys=[encodesCreativeWork_id], backref='encoded_by')
    encodingFormat = Column(String)
    startTime = Column(DateTime)
    endTime = Column(DateTime)
    height = Column(Integer)
    width = Column(Integer)
    requiresSubscription = Column(Boolean)
    uploadDate = Column(DateTime)

    def __repr__(self):
        return "<MediaObject(name='%s', url='%s')>" % (
            self.name, self.url)

    __mapper_args__ = {
        'polymorphic_identity': 'media_objects',
        'inherit_condition': id == CreativeWork.id
    }


class ImageObject(MediaObject):
    __tablename__ = 'image_objects'

    id = Column(Integer, ForeignKey('media_objects.id'), primary_key=True)
    caption = Column(String)
    exifData = Column(String)
    representativeOfPage = Column(Boolean)

    def __repr__(self):
        return "<ImageObject(name='%s', url='%s')>" % (
            self.name, self.url)

    __mapper_args__ = {
        'polymorphic_identity': 'image_objects',
    }


class Article(CreativeWork):
    __tablename__ = 'articles'

    id = Column(Integer, ForeignKey('creative_works.id'), primary_key=True)
    articleBody = Column(String)
    articleSection = Column(String)
    wordCount = Column(Integer)

    def __repr__(self):
        return "<Article(headline='%s', url='%s')>" % (
            self.headline, self.url)

    __mapper_args__ = {
        'polymorphic_identity': 'articles',
    }


class NewsArticle(Article):
    __tablename__ = 'news_articles'

    id = Column(Integer, ForeignKey('articles.id'), primary_key=True)

    def __repr__(self):
        return "<NewsArticle(headline='%s', url='%s')>" % (
            self.headline, self.url)

    __mapper_args__ = {
        'polymorphic_identity': 'news_articles',
    }
