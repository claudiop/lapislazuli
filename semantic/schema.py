class Schema:
    accepted = dict()

    def __init__(self, *args, **kwargs):
        self.data = dict()

    # def __fix_circular_imports(self):
    #     for key, accepted_types in self.accepted.items():
    #         for i, item in enumerate(accepted_types):
    #             if isinstance(item, str):
    #                 accepted_types[i] = importlib.import_module(item)

    def add(self, key, value):
        if key not in self.accepted:
            raise Exception()
        valid_types = self.accepted[key]
        # for i, valid_type in enumerate(valid_types):
        #     if isinstance(valid_type, str):
        #         valid_types[i] = importlib.import_module("." + valid_type)
        is_valid = value.__class__ in valid_types or value.__class__.__name__ in valid_types
        if not is_valid:
            for valid_type in valid_types:
                if isinstance(value, valid_type):
                    is_valid = True
                    break
        if is_valid:
            if key in self.data:
                self.data[key].append(value)
            else:
                self.data[key] = [value, ]
            return
        # for valid_type in valid_types:
        #     try:
        #         if key in self.__data:
        #             self.__data[key].append(valid_type(value))
        #         else:
        #             self.__data[key] = [valid_type(value), ]
        #         return
        #     except Exception:
        #         continue
        raise Exception()

    def get(self, key):
        if key not in self.data:
            raise Exception()
        values = self.data[key]
        if len(values) > 1:
            raise Exception()
        return values[0]

    def dict(self):
        result = dict()
        for key, value in self.data.items():
            key_values = []
            for entry in value:
                if isinstance(entry, Schema):
                    key_values.append(entry.dict())
                else:
                    key_values.append(entry.value)
            result[key] = key_values
        return result

    def get_instances_of(self, clazz):
        results = []
        for value in self.data.values():
            for entry in value:
                if isinstance(entry, clazz):
                    results.append(entry)
        return results

    def __str__(self):
        return str(self.data)

    def __repr__(self):
        return self.data.__repr__()


def module_path(obj):
    if hasattr(obj.__class__, 'type_name'):
        return obj.__module__ + '.' + obj.__class__.type_name
