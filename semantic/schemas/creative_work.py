from semantic.schemas.organization import Organization
from semantic.schemas.person import Person
from semantic.schemas.thing import Thing
from semantic import types


class CreativeWork(Thing):
    schema_name = "CreativeWork"
    accepted = {
        **Thing.accepted,
        **{
            # 'about': [],
            # 'abstract': [types.Text, ],
            # 'accessMode': [types.Text, ],
            # 'accessModeSufficient': [],
            # 'accessibilityAPI': [types.Text, ],
            # 'accessibilityControl': [types.Text, ],
            # 'accessibilityFeature': [types.Text, ],
            # 'accessibilityHazard': [types.Text, ],
            # 'accessibilitySummary': [types.Text, ],
            # 'accountablePerson': [Person, ],
            # 'aggregateRating': [],
            'alternativeHeadline': [types.Text, ],
            # 'associatedMedia': [MediaObject],
            # 'audience': [],
            # 'audio': [AudioObject, Clip], TODO RE-ADD
            'author': [Organization, Person],
            # 'award': [],
            # 'character': [],
            # 'citation': [],
            # 'comment': [],
            # 'commentCount': [],
            # 'conditionsOfAccess': [],
            # 'contentLocation': [],
            # 'contentRating': [],
            # 'contentReferenceTime':  [types.DateTime, ],
            # 'contributor': [],
            'copyrightHolder': [Organization, Person],
            'copyrightYear': [types.Number],
            # 'correction': [],
            # 'creativeWorkStatus': [],
            # 'creator': [],
            'dateCreated': [types.DateTime, types.Date],
            'dateModified': [types.DateTime, types.Date],
            'datePublished': [types.Date],
            # 'discussionUrl': [],
            # 'editor': [],
            # 'educationalAlignment': [],
            # 'educationalUse': [],
            # 'encoding': [],
            # 'encodingFormat': [],
            # 'exampleOfWork': [],
            # 'expires': [],
            # 'funder': [],
            # 'genre': [],????
            # 'hasPart': [],!!!!
            'headline': [types.Text],
            # 'inLanguage': [types.Text], # Or Language
            # 'interactionStatistic': [],
            # 'interactivityType': [],
            'isAccessibleForFree': [],
            'isBasedOn': [types.URL],  # CreativeWorK
            # 'isFamilyFriendly': [],
            # 'isPartOf': [],
            'keywords': [types.Text],
            # 'learningResourceType': [],
            # 'license': [],
            # 'locationCreated': [],
            # 'mainEntity': [],
            # 'material': [],
            # 'materialExtent': [],
            'mentions': [Thing, ],
            # 'offers': [],
            # 'position': [],
            # 'producer': [],
            'provider': [Organization, Person],
            # 'publication': [],
            'publisher': [Organization, Person],
            # 'publisherImprint': [],
            # 'publishingPrinciples': [],
            # 'recordedAt': [],
            # 'releasedEvent': [],
            # 'review': [],
            # 'schemaVersion': [],
            # 'sdDatePublished': [],
            # 'sdLicense': [],
            # 'sdPublisher': [],
            # 'sourceOrganization': [],
            # 'spatial': [],
            # 'spatialCoverage': [],
            # 'sponsor': [],
            # 'temporal': [],
            # 'temporalCoverage': [],
            # 'text': [],?
            'thumbnailUrl': [types.URL, ],
            # 'timeRequired': [],
            # 'translationOfWork': [],
            # 'translator': [],
            # 'typicalAgeRange': [],
            # 'version': [],
            # 'video': [], TODO READD
            # 'workExample': [],
            # 'workTranslation': [],
        }}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class Article(CreativeWork):
    schema_name = "Article"
    accepted = {
        **CreativeWork.accepted,
        **{
            'articleBody': [types.Text, ],
            'articleSection': [types.Text, ],
            # 'backstory': [],
            # 'pageEnd': [],
            # 'pageStart': [],
            # 'pagination': [],
            # 'speakable': [],
            'wordCount': [types.Number, ]
        }}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class NewsArticle(Article):
    schema_name = "NewsArticle"
    accepted = {
        **Article.accepted,
        **{
            # 'dateline': [],
            # 'printColumn': [],
            # 'printEdition': [],
            # 'printPage': [],
            # 'printSection': [],
        }}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
