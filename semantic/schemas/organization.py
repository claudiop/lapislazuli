from semantic.schemas.thing import Thing
from semantic import types


class Organization(Thing):
    schema_name = "Organization"
    accepted = {
        **Thing.accepted,
        **{
            # 'actionableFeedbackPolicy': [types.URL, CreativeWork],
            'address': [types.Text, ],  # PostalAddress],
            # 'alumni': [Person, ],
            # 'areaServed': [],
            # 'award': [types.Text, ],
            # 'brand': [Organization, Brand],
            # 'contactPoint': [],
            # 'correctionsPolicy': [],
            # 'department': [Organization],
            # 'dissolutionDate': [types.Date],
            # 'diversityPolicy': [],
            # 'diversityStaffingReport': [],
            # 'duns': [],
            'email': [types.Text],
            # 'employee': [Person],
            # 'ethicsPolicy': [CreativeWork, types.URL],
            # 'event': [Event],
            # 'faxNumber': [types.Text,],
            # 'founder': [Person],
            # 'foundingDate': [types.Date, ],
            # 'foundingLocation': [Place, ],
            # 'funder': [Organization, Person ],
            # 'globalLocationNumber': [types.Text, ],
            # 'hasCredential': [EducationalOccupationalCredential, ],
            # 'hasOfferCatalog': [OfferCatalog,],
            # 'hasPOS': [Place, ],
            # 'hasProductReturnPolicy': [ProductReturnPolicy, ],
            # 'isicV4': [types.Text, ],
            # 'knowsAbout': [types.Text, Thing, types.URL],
            # 'knowsLanguage': [types.Text, Language],
            # 'legalName': [types.Text,],
            # 'leiCode': [types.Text,],
            # 'location': [Place, PostalAddress, types.Text],
            # 'logo': [ImageObject, types.URL],
            # 'makesOffer': [Offer, ],
            # 'member': [Organization, Person],
            # 'memberOf': [Organization, ProgramMentorship],
            # 'naics': [types.Text, ],
            # 'numberOfEmployees': [QuantitativeValue, ],
            # 'ownershipFundingInfo': [],
            # 'owns': [],
            # 'parentOrganization': [Organization],
            # 'publishingPrinciples': [CreativeWork, types.URL],
            # 'review': [Review],
            # 'seeks': [Demand],
            # 'slogan': [types.Text, ],
            # 'sponsor': [Organization, Person],
            # 'subOrganization': [Organization],
            # 'taxID': [types.Text, ],
            'telephone': [types.Text, ],
            # 'unnamedSourcesPolicy': [],
            # 'vatID': [types.Text, ],
        }}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class NewsMediaOrganization(Organization):
    schema_name = "NewsMediaOrganization"
    accepted = {
        **Organization.accepted,
        **{
            # 'actionableFeedbackPolicy': [types.URL, CreativeWork],
            # ...
        }}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
