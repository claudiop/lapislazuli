from semantic import types
from semantic.schemas.organization import Organization
from semantic.schemas.thing import Thing


class Person(Thing):
    schema_name = "Person"
    accepted = {
        **Thing.accepted,
        **{
            'additionalName': [types.Text, ],
            # 'address': [types.Text, PostalAddress],
            # 'affiliation': [Organization],
            # 'alumniOf': [],
            # 'award': [types.Text, ],
            # 'birthDate': [types.Date], ?
            # 'birthPlace': [Place, ],
            # 'brand': [],
            # 'callSign': [types.Text, ],
            # 'children': [],
            # 'colleague': [],
            # 'contactPoint': [],
            # 'deathDate': [],
            # 'deathPlace': [],
            # 'duns': [types.Text, ],
            'email': [types.Text],
            'familyName': [types.Text, ],
            # 'faxNumber': [],
            # 'follows': [],
            # 'funder': [],
            # 'gender': [],
            'givenName': [types.Text],
            # 'globalLocationNumber': [],
            # 'hasCredential': [],
            # 'hasOccupation': [],
            # 'hasOfferCatalog': [],
            # 'hasPOS': [],
            # 'height': [],
            # 'homeLocation': [],
            # 'honorificPrefix': [],
            # 'honorificSuffix': [],
            # 'isicV4': [],
            # 'jobTitle': [],
            # 'knows': [Person, ],
            # 'knowsAbout': [types.Text, Thing, types.URL],
            'knowsLanguage': [types.Text],  # TODO Language
            # 'makesOffer': [Offer, ],
            # 'naics': [types.Text, ],
            # 'nationality': [Country],
            # 'netWorth': [MonetaryAmount, PriceSpecification],
            # 'owns': [OwnershipInfo, Product],
            # 'parent': [Person, ],
            # 'performerIn': [Event, ],
            # 'publishingPrinciples': [],
            # 'relatedTo': [Person, ],
            # 'seeks': [Demand, ],
            # 'sibling': [Person, ],
            # 'sponsor': [Organization, Person],
            # 'spouse': [Person, ],
            # 'taxID': [type.Text, ],
            'telephone': [types.Text],
            # 'vatID': [],
            # 'weight': [],
            # 'workLocation': [],
            'worksFor': [Organization, ],
        }}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
