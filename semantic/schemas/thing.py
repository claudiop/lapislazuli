from semantic.schema import Schema
from semantic import types


class Thing(Schema):
    schema_name = "Thing"
    accepted = {
        # 'additionalType': [types.URL, ],
        # 'alternateName': [types.Text, ],
        'description': [types.Text, ],  # ?
        # 'disambiguatingDescription': [types.Text, ],
        # 'identifier': [types.URL, types.Text],
        'image': [types.URL, "ImageObject"],
        # 'mainEntityOfPage': [types.URL, ],
        'name': [types.Text, ],
        # 'action': [],
        # 'sameAs': [types.URL, ],!!
        # 'subjectOf': [],
        'url': [types.URL, ],
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
