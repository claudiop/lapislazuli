from semantic import types
from semantic.schemas.creative_work import CreativeWork, NewsArticle


class MediaObject(CreativeWork):
    schema_name = "MediaObject"
    accepted = {
        **CreativeWork.accepted,
        **{
            'associatedArticle': [NewsArticle, ],
            'bitrate': [types.Text, ],
            'contentSize': [types.Text, ],
            'contentUrl': [types.URL],
            # 'duration': [Duration,],
            'embedUrl': [types.URL, ],
            'encodesCreativeWork': [CreativeWork, ],
            'encodingFormat': [types.Text, types.URL],
            'endTime': [types.DateTime, types.Text],
            'height': [types.Number],
            'playerType': [types.Text],
            # 'productionCompany': [Organization],
            # 'regionsAllowed': [Place],
            'requiresSubscription': [types.Boolean, ],
            'startTime': [types.DateTime, types.Time],
            'uploadDate': [types.Date],
            'width': [types.Number],
        }}

    def __init__(self, *args, **kwargs):
        super().__init__("MediaObject", *args, **kwargs)


class ImageObject(MediaObject):
    schema_name = "ImageObject"
    accepted = {
        **MediaObject.accepted,
        **{
            'caption': [types.Text, ],
            'exifData': [types.Text, ],
            'representativeOfPage': [types.Boolean, ],
            # 'thumbnail': [],
        }}

    def __init__(self, *args, **kwargs):
        super().__init__("MediaObject", *args, **kwargs)
