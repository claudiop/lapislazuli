import re
from datetime import datetime, time, date


class Value:
    type_name = "Unknown"

    def __init__(self, value, *args, **kwargs):
        self.value = value

    def __eq__(self, other):
        if not (isinstance(other, self.__class__)):
            return False
        return self.value == other.value

    def __str__(self):
        return str(self.value)

    def __repr__(self):
        return self.value.__repr__()

    def __hash__(self):
        return self.value.__hash__()

    @staticmethod
    def accepts(value):
        try:
            super(value)
            return True
        except Exception:
            return False


class Boolean(Value):
    type_name = "Boolean"

    def __init__(self, value, *args, **kwargs):
        super().__init__(value, *args, **kwargs)
        if not isinstance(value, bool):
            raise Exception(f"{value} is not a Boolean")

    # def __str__(self):
    #     return str(self.value)


class Text(Value):
    type_name = "Text"

    def __init__(self, value, *args, **kwargs):
        super().__init__(value, *args, **kwargs)
        if not isinstance(value, str):
            raise Exception(f"{value} is not a Text")

    # def __str__(self):
    #     return str(self.value)


class Number(Value):
    type_name = "Number"

    def __init__(self, value, *args, **kwargs):
        super().__init__(value, *args, **kwargs)
        if not (isinstance(value, int) or isinstance(value, float)):
            raise Exception(f"{value} is not a Number")

    # def __str__(self):
    #     return str(self.value)


class Date(Value):
    type_name = "Date"

    def __init__(self, value, *args, **kwargs):
        super().__init__(value, *args, **kwargs)
        if not isinstance(value, date):
            raise Exception(f"{value} is not a Date")

    # def __str__(self):
    #     return str(self.value)


class Time(Value):
    type_name = "Time"

    def __init__(self, value, *args, **kwargs):
        super().__init__(value, *args, **kwargs)
        if not isinstance(value, time):
            raise Exception(f"{value} is not a Time")

    # def __str__(self):
    #     return str(self.value)


class DateTime(Value):
    type_name = "DateTime"

    def __init__(self, value, *args, **kwargs):
        super().__init__(value, *args, **kwargs)
        if not isinstance(value, datetime):
            raise Exception(f"{value} is not a DateTime")

    # def __str__(self):
    #     return str(self.value)


_URL_REGEX = re.compile("^http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*(),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+$")


class URL(Text):
    type_name = "URL"

    def __init__(self, value, *args, **kwargs):
        super().__init__(value, *args, **kwargs)
        # if _URL_REGEX.search(value) is None:
        #     raise Exception(f"{value} is not an URL")

    # def __str__(self):
    #     return str(self.value)

# # Pseudo hack to avoid circular imports
# class SchemaWrapper(Value):
#     type_name = "URL"
#
#     def __init__(self, value, *args, **kwargs):
#         super().__init__(value, *args, **kwargs)
#         if not isinstance(value, datetime):
#             raise Exception(f"{value} is not a DateTime")
#
