import datetime
import logging
import threading
from time import sleep
from multiprocessing import Process
import pymongo
from scrapy.settings import Settings

from scrapy.crawler import CrawlerProcess
import settings
from spiders.cmjornal import CMJornalSpider
from spiders.jn import JNSpider
from spiders.nit import NitSpider
from spiders.publico import PublicoSpider
from spiders.observador import ObservadorSpider
from spiders.destak import DestakSpider
from spiders.lusa import LusaSpider
from spiders.dnjornal import DNJornalSpider
from spiders.expresso import ExpressoSpider
from spiders.sapo import SapoSpider
from spiders.rtp import RtpSpider

logging.getLogger('scrapy.spidermiddlewares.httperror').setLevel("INFO")

crawler_settings = Settings()
crawler_settings.setmodule(settings)
process = CrawlerProcess(crawler_settings)


def collect_sample():
    process.crawl(PublicoSpider, max_page=20)
    process.crawl(JNSpider, 'cookie-val', max_page=20)
    process.crawl(NitSpider, 'nounce-val', quick=True)
    process.crawl(
        CMJornalSpider,
        min_date=datetime.datetime.now().date() - datetime.timedelta(days=7))
    process.crawl(
        PublicoSpider,
        min_date=datetime.datetime.now() - datetime.timedelta(days=7))
    process.crawl(
        ObservadorSpider,
        min_date=datetime.datetime.now().date() - datetime.timedelta(days=7))
    process.crawl(
        DNJornalSpider,
        min_date=datetime.datetime.now().date() - datetime.timedelta(days=7))
    process.crawl(
        ExpressoSpider,
        min_date=datetime.datetime.now().date() - datetime.timedelta(days=7))
    process.crawl(
        SapoSpider,
        min_date=datetime.datetime.now().date() - datetime.timedelta(days=7))
    process.crawl(RtpSpider, min_id=1000000, man_id=1001000)
    process.crawl(DestakSpider, min_id=100000, man_id=101000)
    process.crawl(LusaSpider, min_id=1000000, max_id=1001000)
    process.start()


if __name__ == "__main__":
    collect_sample()
